
Trying it out -

### Docker version of Fuseki

* Get the Docker image for Fuseki2 :

sudo docker pull stain/jena-fuseki

* Set up a Busybox container to offer persistent storage volume :

sudo docker run --name fuseki-data -v /fuseki busybox

* Run a Fuseki container associated with volume:

sudo docker run -d --name fuseki -p 3030:3030 -e ADMIN_PASSWORD=pw123 --volumes-from fuseki-data stain/jena-fuseki

* Open Fuseki : http://localhost:3030
* login:  admin pw123
* create dataset
* upload data files (some Turtle...)
