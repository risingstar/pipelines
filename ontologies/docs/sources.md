Current per-company RDF, eg.
http://data.companieshouse.gov.uk/doc/company/02050399.rdf

In Alpha:
http://business.data.gov.uk/companies/docs/data-model-reference.html

SPARQL endpoint (in pre-Alpha):
http://business.data.gov.uk/companies/app/explore/sparql.html


Vocabs:

https://schema.org/

http://datashapes.org/schemadoc/Organization.html

// Handy Utilities:

https://pdftables.com/

https://www.tablesgenerator.com/markdown_tables
