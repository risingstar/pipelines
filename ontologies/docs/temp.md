<http://business.data.gov.uk/id/company/02050399>
         skos:prefLabel             ?prefLabel
   ;     rov:legalName              ?legalName
   ;     org:identifier             ?identifier
   ;     rov:orgType                ?orgType
   ;     rov:registration           ?registration
   ;     rov:orgStatus              ?orgStatus
   ;     terms:accountsSchedule  ?accountsSchedule
   ;     terms:countryOfOrigin   ?countryOfOrigin
   ;     terms:incorporationDate ?incorporationDate
   ;     terms:registeredAddress ?registeredAddress
   ;     terms:returnsSchedule   ?returnsSchedule
   .
?registeredAddress
         vcard:organization-name    ?organizationName
   ;     vcard:street-address       ?streetAddress
   ;     vcard:locality             ?locality
   ;     postcode:postcode          ?postcode
   ;     vcard:extended-address     ?extendedAddress
   .
}
terms:countryOfOrigin    "United Kingdom"@en ;
terms:incorporationDate  "1986-08-28"^^xsd:date ;
terms:mortgages          <http://business.data.gov.uk/companies/profile/02050399/mortgages> ;
terms:registeredAddress  <http://business.data.gov.uk/companies/profile/02050399/registered-address> ;
terms:returnsSchedule    <http://business.data.gov.uk/companies/profile/02050399/returns-schedule> ;
rov:orgActivity          <http://business.data.gov.uk/companies/def/sic-2007/70100> ;
rov:orgStatus            <http://business.data.gov.uk/companies/def/company-status/0> ;
rov:orgType              category:private-limited-company ;
rov:registration         <http://business.data.gov.uk/companies/profile/02050399/registration> .
