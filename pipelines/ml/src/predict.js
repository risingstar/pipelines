/**
 * Use a model to predict values for each item in the input data.
 */
const path = require('path')
const debug = require('debug')('predict')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')

const {
  HYPER_LEARNING_RATE,
  HYPER_LOSS,
  MODEL_PATH,
  NORMALISE_FN_PATH,
  TASK_ID,
  TEMP_PATH
} = process.env

const { featureColumns, FormatPredictionFn } = require(NORMALISE_FN_PATH)

/**
 * Creates a single pipeline to make predictions.
 */
const main = async () => {
  const inputFile = path.join(TEMP_PATH, `${TASK_ID}-normalise.csv`)
  const modelFile = `file://${MODEL_PATH}/model.json`
  const outputFile = path.join(TEMP_PATH, `${TASK_ID}-predict.json`)

  debug(`input file '${inputFile}'`)
  debug(`model file '${modelFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    async setup() {
      const model = await tf.loadModel(modelFile)
      const optimizer = tf.train.adam(+HYPER_LEARNING_RATE)

      model.compile({
        optimizer,
        loss: HYPER_LOSS,
        metrics: ['accuracy']
      })
      this.model = model
    }

    async processElement(c) {
      const input = c.element()

      /**
       * Establish the feature columns for the inputs:
       */
      const features = featureColumns(input)

      const predictXs = tf.tensor2d([features[1]])
      const prediction = this.model.predict(predictXs)
      const argMax = await prediction.argMax(1).data()
      const data = await prediction.data()
      const risk = data[1]

      c.output({
        label: features[0],
        risk,
        input
      })
    }
  }))
  .apply(ParDo.of(new FormatPredictionFn()))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
