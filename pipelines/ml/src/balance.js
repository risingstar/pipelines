/**
 * Apply a full-pass transform to balance the classes.
 */
const fs = require('fs')
const path = require('path')
const debug = require('debug')('balance')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const {
  FIELD_TO_BALANCE,
  MULTIPLIER,
  TASK_ID,
  TEMP_PATH,
  VALUE_TO_BALANCE
} = process.env

/**
 * Creates a single pipeline to balance the classes.
 */
const main = async () => {
  const inputFile = path.resolve(TEMP_PATH, `${TASK_ID}-normalise.csv`)
  const outputFile = path.resolve(TEMP_PATH, `${TASK_ID}-balance.csv`)

  debug(`input file '${inputFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    setup() {
      this.header = false
    }

    processElement(c) {
      const input = c.element()
      const { status, category, type, rural } = input

      if (!this.header) {
        c.output('status,category,type,rural\n')
        this.header = true
      }

      /**
       * Boost the row if it matches the boost criteria:
       */
      if (input[FIELD_TO_BALANCE] === VALUE_TO_BALANCE) {
        for (let i = 0; i < +MULTIPLIER; i++) {
          c.output(`${status},${category},${type},${rural}\n`)
        }
      }
      c.output(`${status},${category},${type},${rural}\n`)
    }
  }))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
