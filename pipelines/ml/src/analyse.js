/**
 * Apply a full-pass transform in order to deduce properties of the fields.
 */
const path = require('path')
const debug = require('debug')('analyse')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const numeral = require('numeral')

const {
  INPUT_PATH,
  TASK_ID,
  TEMP_PATH
} = process.env

/**
 * Creates a single pipeline to analyse the data.
 */
const main = async () => {
  const inputFile = path.resolve(INPUT_PATH, `${TASK_ID}.csv`)
  const outputFile = path.resolve(TEMP_PATH, `${TASK_ID}-analyse.csv`)

  debug(`input file '${inputFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    setup() {
      this.fields = new Map()
      this.skipFields = []
    }

    processElement(c) {
      const input = c.element()
      const fields = this.fields

      for (const [k, v] of Object.entries(input)) {
        /**
         * If this field should be skipped then there is no more work to do:
         */
        if (this.skipFields.includes(k)) continue

        /**
         * First check that we have a map for the metadata we want to track:
         */
        if (!fields.has(k)) {
          /**
           * Now create a map of all the meta values we want to track, with
           * their initial values:
           */
          fields.set(k, new Map([

            /**
             * Track the number of values:
             */
            ['count', 0],

            /**
             * Track the number of missing values:
             */
            ['missing', 0],

            /**
             * Track the number of values that could be regarded as numbers:
             */
            ['number', 0],

            /**
             * Track the range of values:
             */
            ['range', new Map()]
          ]))
        }

        /**
         * A shortcut for the metadata we're tracking for this field.
         */
        const field = fields.get(k)
        field.set('count', field.get('count') + 1)

        /**
         * Track the number of missing values:
         */
        if (!v || v.trim() === '') {
          field.set('missing', field.get('missing') + 1)
        }

        /**
         * Track the number of values that could be interpreted as a number:
         */
        if (numeral(v).value() !== null) {
          field.set('number', field.get('number') + 1)
        }

        /**
         * Track the range of values:
         */
        const range = field.get('range')
        range.set(v, (range.get(v) || 0) + 1)
      }
    }

    finishBundle(fbc) {
      fbc.output('Field Name,count,missing,number,unique\n')
      for (const [key, meta] of this.fields) {
        fbc.output(`${key},${meta.get('count')},${meta.get('missing')},${meta.get('number')},${meta.get('range').size}\n`)
        for (const [value, count] of meta.get('range')) {
          fbc.output(`${key},${value},${count}\n`)
        }
      }
    }
  }))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
