/**
 * Train a model using the input data.
 */
const path = require('path')
const debug = require('debug')('train')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')

const {
  HYPER_EPOCHS,
  HYPER_INPUT_DIM,
  HYPER_LEARNING_RATE,
  HYPER_LOSS,
  MODEL_PATH,
  NORMALISE_FN_PATH,
  TASK_ID,
  TEMP_PATH
} = process.env

const { featureColumns } = require(NORMALISE_FN_PATH)

/**
 * Creates a single pipeline to train the model.
 */
const main = async () => {
  const inputFile = path.join(TEMP_PATH, `${TASK_ID}-partition-train.csv`)
  const modelPath = `file://${MODEL_PATH}`
  const outputFile = path.join(TEMP_PATH, `${TASK_ID}-predict.csv`)

  debug(`input file '${inputFile}'`)
  debug(`model path '${modelPath}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    /**
     * Fits the model using the X's and the Y's.
     */
    fit(xs, ys) {
      const options = {
        epochs: HYPER_EPOCHS,
        validationSplit: 0.2,
        // shuffle: true,
        callbacks: {
          onEpochEnd: (epoch, log) => {
            console.log(`Epoch ${epoch}: loss = ${JSON.stringify(log)}`)
          }
        }
      }
      return this.model.fit(xs, ys, options)
    }
    /**
     * Predicts something.
     */
    predict(xs) {
      const predictXs = tf.tensor2d(xs)
      return this.model.predict(predictXs)
    }

    setup() {
      this.features = []
      this.labels = []
    }

    processElement(c) {
      const input = c.element()

      /**
       * Establish the feature columns for the inputs:
       */
      const features = featureColumns(input)

      this.labels.push(features[0])
      this.features.push(features[1])
    }

    async finishBundle(fbc) {

      /**
       * Create the x's as a 2-D tensor from the features:
       */
      const xs = tf.tensor2d(this.features)

      /**
       * Create the y's as a one-hot from the labels:
       *
       * Note that the type of the 1-D tensor must be int32.
       */
      const labelsTensor = tf.tensor1d(this.labels, 'int32')
      const ys = tf.oneHot(labelsTensor, 2)
      labelsTensor.dispose()

      /**
       * Create a sequential model with:
       *
       *  * a hidden layer with 16 nodes, using the sigmoid activation
       *    function;
       *  * an output layer with 2 outputs (active and liquidation),
       *    using the softmax activation function.
       */
      const model = tf.sequential()

      const hidden = tf.layers.dense({
        units: 16,
        activation: 'sigmoid',
        inputDim: +HYPER_INPUT_DIM
      })
      const output = tf.layers.dense({
        units: 2,
        activation: 'softmax'
      })

      model.add(hidden)
      model.add(output)

      /**
       * The optimizer.
       */
      const optimizer = tf.train.adam(+HYPER_LEARNING_RATE)
      /**
       * Compile the model:
       */
      model.compile({
        optimizer,
        loss: HYPER_LOSS,
        metrics: ['accuracy']
      })

      /**
       * Make the model available to the fit() and predict() methods:
       */
      this.model = model

      /**
       * Train the model:
       */
      const f = await this.fit(xs, ys)
      console.log(f)

      await model.save(modelPath)
      /**
       * Create predictions for all permutations:
       */
      fbc.output('category,type,rural,predict active,predict inactive\n')
      // for (let category = 0; category <= Math.max(...orgCategoryMap.values()); category++) {
      //   for (let type = 0; type <= Math.max(...orgTypeMap.values()); type++) {
      //     for (let ru11 = 0; ru11 <= Math.max(...orgRurIndMap.values()); ru11++) {
      //       let p = await this.predict([
      //         [orgCategoryMap.scale0To1(category), orgTypeMap.scale0To1(type), orgRurIndMap.scale0To1(ru11)]
      //       ]).data()
      //       fbc.output(`${category},${type},${ru11},${Math.floor(p[0] * 100)},${Math.floor(p[1] * 100)}\n`)
      //     }
      //   }
      // }
    }
  }))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
