/**
 * Evaluate a model using the input data.
 */
const path = require('path')
const debug = require('debug')('confusion-matrix')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')

const {
  HYPER_LEARNING_RATE,
  HYPER_LOSS,
  MODEL_PATH,
  NORMALISE_FN_PATH,
  TASK_ID,
  TEMP_PATH
} = process.env

const { featureColumns } = require(NORMALISE_FN_PATH)

/**
 * Creates a single pipeline to evaluate the model.
 */
const main = async () => {
  const inputFile = path.join(TEMP_PATH, `${TASK_ID}-partition-evaluate.csv`)
  const modelFile = `file://${MODEL_PATH}/model.json`
  const outputFile = path.join(TEMP_PATH, `${TASK_ID}-confusion-matrix.csv`)

  debug(`input file '${inputFile}'`)
  debug(`model file '${modelFile}'`)
  debug(`output file '${outputFile}'`)

  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(inputFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    async setup() {
      const model = await tf.loadModel(modelFile)
      const optimizer = tf.train.adam(+HYPER_LEARNING_RATE)

      model.compile({
        optimizer,
        loss: HYPER_LOSS,
        metrics: ['accuracy']
      })
      this.model = model

      this.matrix = [
        [0, 0],
        [0, 0]
      ]
    }

    async processElement(c) {
      const input = c.element()

      /**
       * Establish the feature columns for the inputs:
       */
      const features = featureColumns(input)

      const predictXs = tf.tensor2d([features[1]])
      const prediction = this.model.predict(predictXs)
      const argMax = await prediction.argMax(1).data()

      /**
       * If argMax == features[0] then we have a TP or a TN, otherwise
       * we have a FP or a FN:
       */
      this.matrix[features[0]][+argMax]++
    }

    async finishBundle(fbc) {
      const ACTIVE = 0
      const INACTIVE = 1
      /**
       * The true positives are entries that are active, and that the
       * model correctly predicted were active:
       */
      const tp = this.matrix[ACTIVE][ACTIVE]
      /**
       * The false positives are entries that are inactive, but the
       * model predicted that they were active:
       */
      const fp = this.matrix[INACTIVE][ACTIVE]
      /**
       * The false negatives are entries that are active, but the
       * model predicted that they are inactive:
       */
      const fn = this.matrix[ACTIVE][INACTIVE]
      /**
       * And finall, the true negatives are entries that are inactive,
       * and the model has correctly predicted that they are inactive:
       */
      const tn = this.matrix[INACTIVE][INACTIVE]

      fbc.output('label,active,inactive\n')
      fbc.output(`active,${tp},${fn}\n`)
      fbc.output(`inactive,${fp},${tn}\n`)
      /**
       * The total number of entries:
       */
      const total = tp + fp + tn + fn
      /**
       * The accuracy is the rate of making a correct prediction:
       */
      const accuracy = (tp + tn) / total
      /**
       * Recall (or sensitivity) is the rate of all positives
       * correctly predicted from all of the positives:
       */
      const recall = tp / (tp + fn)
      /**
       * Precision is the rate of positive predictions that are
       * correct:
       */
      const precision = tp / (tp + fp)
      /**
       * The F1 rate combines precision and recall to give an overall
       * assessment of the predictions:
       */
      const f1 = 1 / ((1 / recall) + (1 / precision) / 2)
      /**
       * Specificity is the true negative rate, i.e., how good is
       * the model at predicting negatives:
       */
      const specificity = tn / (tn + fp)
      /**
       * Prevalance is an indication of the rate that positives
       * are represented in the data:
       */
      const prevalance = tp / total

      fbc.output(`# accuracy: ${accuracy}\n`)
      fbc.output(`# recall: ${recall}\n`)
      fbc.output(`# precision: ${precision}\n`)
      fbc.output(`# f1: ${f1}\n`)
      fbc.output(`# specificity: ${specificity}\n`)
      fbc.output(`# prevalance: ${prevalance}\n`)
    }
  }))
  .apply(
    ParDo.of(new FileWriterFn(outputFile))
  )
  await p.run().waitUntilFinish()
  debug('complete')
}

main()
