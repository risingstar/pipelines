const debug = require('debug')('postcode-to-output-area')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const ElasticSearchWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/ElasticSearchWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const RequestReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/RequestReaderFn')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const waitOn = require('wait-on')

const {
  ELASTICSEARCH_HOST,
  ONS_POSTCODE_SOURCE_URL
} = process.env

const main = async () => {
  debug('about to wait for resources')
  await waitOn({
    resources: [
      ELASTICSEARCH_HOST,
      new URL(ONS_POSTCODE_SOURCE_URL).origin
    ],
    /**
     * No point in waiting too long:
     */
    timeout: 10 * 1000,
    /**
     * If debug is enabled for this module then use verbose
     * logging in wait-on:
     */
    verbose: debug.enabled
  })
  debug('resources ready')

  /**
   * Retrieve the list of postcode mappings and process as a
   * CSV file:
   */
  let p = Pipeline.create()
  p
  .apply(
    ParDo.of(
      new RequestReaderFn(ONS_POSTCODE_SOURCE_URL)
    )
  )
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))

  /**
   * Write to ElasticSearch:
   */
  .apply(
    ParDo.of(new ElasticSearchWriterFn({
      connection: {
        host: ELASTICSEARCH_HOST
      },
      idFn: obj => obj.pcd.replace(' ', ''),
      type: 'PostCode',
      index: 'datalake-ons-postcode'
    }))
  )

  await p.run().waitUntilFinish()
  debug('pipeline complete')
}

main()