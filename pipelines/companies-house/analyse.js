/**
 * Apply a full-pass transform in order to deduce ranges for various fields.
 */
const path = require('path')
const debug = require('debug')('analyse')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')

const waitOn = require('wait-on')

/**
 * Grab environment variables:
 *
 *  - the year, month and day to process the files for;
 *  - the number of parts that make up the file:
 */
const YEAR = process.env.COMPANIES_HOUSE_FILES_YEAR
const MONTH = process.env.COMPANIES_HOUSE_FILES_MONTH
const DAY = process.env.COMPANIES_HOUSE_FILES_DAY
const PARTS = process.env.COMPANIES_HOUSE_FILES_PARTS

/**
 * Creates a single pipeline to analyse the data, for a specified
 * month.
 */
const analyse = async (year, month, day, _part, parts) => {
  const part = _part + 1
  const dbg = debug.extend(`basicCompanyData: ${year}-${month} part ${part}`)

  const csvFile = `/tmp/risingstar/BasicCompanyData/${year}-${month}/csv/part${part}_${parts}.csv`
  const rangeFile = `/tmp/risingstar/BasicCompanyData/${year}-${month}/csv/part${part}_${parts}.range.csv`

  dbg(`about to analyse csv file ${csvFile}`)
  const p = Pipeline.create()

  p
  .apply(ParDo.of(new FileReaderFn(csvFile)))
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    setup() {
      const ranges = new Map()

      ranges.set('orgStatus', new Map())
      ranges.set('orgType', new Map())
      this.ranges = ranges
    }

    processElement(c) {
      const input = c.element()

      /**
       * Track the range of possible values for the status and type:
       */
      this.ranges.get('orgStatus').set(input.CompanyStatus, '')
      this.ranges.get('orgType').set(input.CompanyCategory, '')
    }

    finishBundle(fbc) {
      for (const [key, range] of this.ranges) {
        for (const value of range.keys()) {
          fbc.output(`${key}: ${value}\n`)
        }
      }
    }
  }))
  .apply(
    ParDo.of(new FileWriterFn(rangeFile))
  )
  await p.run().waitUntilFinish()
  dbg('analyse complete')
}

const main = async () => {

  /**
   * Create as many pipelines as there are parts for the file:
   */
  for (const i of [...Array(+PARTS).keys()]) {
    try {
      await analyse(YEAR, MONTH, DAY, i, PARTS)
    } catch(err) {
      console.error(`Error in part ${i} of ${PARTS}: ${err}`)
    }
  }
}

main()
