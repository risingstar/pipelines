const path = require('path')
const debug = require('debug')('basic-company-data-monthly')

const Csv = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Csv')
const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const ElasticSearchWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/ElasticSearchWriterFn')
const FileReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileReaderFn')
const FileWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/FileWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const RequestReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/RequestReaderFn')
const Split = require('beamish-direct-runner/lib/sdk/transforms/node-streams/Split')
const UnzipReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/UnzipReaderFn')

const fs = require('fs')

const waitOn = require('wait-on')
const moment = require('moment')

/**
 * Grab environment variables:
 *
 *  - the server that we'll pick the zip files up from;
 *  - the year and month to get the files for;
 *  - the number of parts that make up the file:
 */

const SERVER_HOST = process.env.COMPANIES_HOUSE_FILES_URL
const YEAR = process.env.COMPANIES_HOUSE_FILES_YEAR
const MONTH = process.env.COMPANIES_HOUSE_FILES_MONTH
const PARTS = process.env.COMPANIES_HOUSE_FILES_PARTS
const ELASTICSEARCH_HOST = process.env.ELASTICSEARCH_HOST

/**
 * Create a single pipeline to download one part of the whole, for a specified
 * month:
 */

const basicCompanyData = async (year, month, _part, parts) => {
  const part = _part + 1
  const dbg = debug.extend(`basicCompanyData: ${year}-${month} part ${part}`)

  /**
   * Download a Zip file from Companies House. The URLs look like this:
   *
   * http://download.companieshouse.gov.uk/BasicCompanyData-2018-09-01-part1_5.zip
   * http://download.companieshouse.gov.uk/BasicCompanyData-2018-09-01-part2_5.zip
   * ...
   * http://download.companieshouse.gov.uk/BasicCompanyData-2018-09-01-part5_5.zip
   */

  const tmpFile = `temp-${year}-${month}-${part}.zip`
  dbg(`about to download zip file to ${tmpFile}`)

  let p = Pipeline.create()

  p
  .apply(
    ParDo.of(
      new RequestReaderFn(
        `${SERVER_HOST}/BasicCompanyData-${year}-${month}-01-part${part}_${parts}.zip`
      )
    )
  )
  .apply(
    ParDo.of(new FileWriterFn(tmpFile))
  )

  await p.run().waitUntilFinish()
  dbg('download complete')

  dbg('about to import zip file')
  p = Pipeline.create()

  p
  .apply(
    ParDo.of(
      new UnzipReaderFn(tmpFile)
    )
  )
  .apply(ParDo.of(new Split()))
  .apply(ParDo.of(new Csv(true)))
  .apply(ParDo.of(new class extends DoFn {
    processElement(c) {
      const input = c.element()

      ;[
        'Accounts.LastMadeUpDate',
        'Accounts.NextDueDate',
        'ConfStmtNextDueDate',
        'ConfStmtLastMadeUpDate',
        'DissolutionDate',
        'IncorporationDate',
        'Returns.LastMadeUpDate',
        'Returns.NextDueDate',
        'PreviousName_1.CONDATE',
        'PreviousName_2.CONDATE',
        'PreviousName_3.CONDATE',
        'PreviousName_4.CONDATE',
        'PreviousName_5.CONDATE',
        'PreviousName_6.CONDATE',
        'PreviousName_7.CONDATE',
        'PreviousName_8.CONDATE',
        'PreviousName_9.CONDATE',
        'PreviousName_10.CONDATE'
      ].forEach(fieldName => {
        const s = input[fieldName]

        delete input[fieldName]

        if (s !== '') {
          const d = moment(s, 'DD/MM/YYYY')

          if (!d.isValid()) {
            console.error(`company ${input.CompanyNumber}: date in field '${fieldName}' is invalid (${s})`)
          } else {
            input[fieldName] = d.format('YYYY-MM-DD')
          }
        }
      })
      c.output(input)
    }
  }))
  .apply(
    ParDo.of(new ElasticSearchWriterFn({
      connection: {
        host: ELASTICSEARCH_HOST
      },
      idFn: obj => obj.CompanyNumber,
      type: 'Company',
      index: `datalake-companieshouse-company-${year}-${month}`
    }))
  )

  await p.run().waitUntilFinish()
  dbg('import complete')
}


waitOn(
  {
    resources: [
      SERVER_HOST,
      ELASTICSEARCH_HOST
    ],
    timeout: 5000
  },
  async err => {
    if (err) { throw new Error(err) }

    /**
     * Create as many pipelines as there are parts for the file:
     */

    for (const i of [...Array(PARTS - 1).keys()]) {
      try {
        await basicCompanyData(YEAR, MONTH, i, PARTS)
      } catch(err) {
        console.error(`Error in part ${i} of ${PARTS}: ${err}`)
      }
    }
  }
)