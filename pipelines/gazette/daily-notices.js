const debug = require('debug')('gazette:daily-notices')

const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')
const ElasticSearchWriterFn = require('beamish-direct-runner/lib/sdk/io/node-streams/ElasticSearchWriterFn')
const ParDo = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/ParDo')
const Pipeline = require('beamish-direct-runner/lib/sdk/NodeStreamsPipeline')
const RequestReaderFn = require('beamish-direct-runner/lib/sdk/io/node-streams/RequestReaderFn')
const RequestTransformFn = require('beamish-direct-runner/lib/sdk/io/node-streams/RequestTransformFn')
const waitOn = require('wait-on')
const N3 = require('n3')
const parser = new N3.Parser()

const {
  GAZETTE_API_HOST,
  GAZETTE_CATEGORY_CODE,
  GAZETTE_RESULTS_PAGE_SIZE,
  GAZETTE_START_PUBLISH_DATE,
  ELASTICSEARCH_HOST
} = process.env
const gazetteUrl = `${GAZETTE_API_HOST}/insolvency/notice/data.json?start-publish-date=${GAZETTE_START_PUBLISH_DATE}&categorycode=${GAZETTE_CATEGORY_CODE}&results-page-size=${GAZETTE_RESULTS_PAGE_SIZE}`

const main = async () => {
  debug('about to wait for resources')
  await waitOn({
    resources: [
      GAZETTE_API_HOST.replace('https', 'https-get'),
      ELASTICSEARCH_HOST
    ],
    /**
     * No point in waiting too long:
     */
    timeout: 10 * 1000,
    /**
     * If debug is enabled for this module then use verbose
     * logging in wait-on:
     */
    verbose: debug.enabled
  })
  debug('resources ready')

  /**
   * Retrieve the list of notices for the specified date:
   */
  const p = Pipeline.create()
  p
  .apply(
    ParDo.of(
      new RequestReaderFn(gazetteUrl, true)
    )
  )

  /**
   * Step through the notice summaries and get the URL from
   * which we can get the full details. We're going to use
   * the TTL files:
   */
  .apply(ParDo.of(new class extends DoFn {
    processElement(c) {
      const input = c.element()
      for (const elem of input.entry) {
        c.output(elem)
      }
    }
  }))
  .apply(ParDo.of(new class extends DoFn {
    processElement(c) {
      const input = c.element()
      for (const link of input.link) {
        c.output(link)
      }
    }
  }))
  .apply(ParDo.of(new class extends DoFn {
    processElement(c) {
      const input = c.element()
      if (input['@title'] === 'TURTLE' && !input['@href'].includes('?')) {
        c.output(input['@href'])
      }
    }
  }))

  /**
   * Use each URL to retrieve the TTL file, parse, and then
   * output all of the triples:
   */
  .apply(
    ParDo.of(
      new RequestTransformFn()
    )
  )
  .apply(ParDo.of(new class extends DoFn {
    async processElement(c) {
      const input = c.element()
      const f = await new Promise((resolve, reject) => {
        const triples = parser.parse(input, (err, quad, prefixes) => {
          if (err) return resolve(err)
          if (prefixes) return resolve()
          c.output(quad)
        })
      })
    }
  }))

  /**
   * The only quads we're interested in are the values for
   * gazorg:companyNumber:
   */
  .apply(ParDo.of(new class extends DoFn {
    async processElement(c) {
      const input = c.element()
      if (input.predicate.value === 'https://www.thegazette.co.uk/def/organisation#companyNumber') {
        c.output(input)
      }
    }
  }))
  .apply(ParDo.of(new class extends DoFn {
    async processElement(c) {
      const input = c.element()
      c.output(input.object.value)
    }
  }))
  .apply(ParDo.of(new class extends DoFn {
    async processElement(c) {
      const input = c.element()
      c.output(input.trim())
    }
  }))

  /**
   * There can be multiple notices per company so remove duplicate
   * company numbers:
   */
  .apply(ParDo.of(new class extends DoFn {
    setup() {
      this.companyNumbers = new Set()
    }

    processElement(c) {
      const input = c.element()
      this.companyNumbers.add(input)
    }

    finishBundle(fbc) {
      for (const companyNumber of this.companyNumbers.values()) {
        fbc.output(companyNumber)
      }
    }
  }))

  /**
   * Now create a derived value for each company that indicates
   * it is entering liquidation:
   */
  .apply(ParDo.of(new class extends DoFn {
    async processElement(c) {
      const input = c.element()
      c.output({
        id: input,
        inLiquidation: true
      })
    }
  }))

  /**
   * Save the status to ES:
   */
  .apply(ParDo.of(new class extends DoFn {
    processElement(c) {
      const input = c.element()
      debug(`writing: ${JSON.stringify(input)}`)
      c.output(input)
    }
  }))
  .apply(
    ParDo.of(new ElasticSearchWriterFn({
      connection: {
        host: ELASTICSEARCH_HOST
      },
      idFn: obj => obj.id,
      type: 'LiquidationStatus',
      index: 'datalake-gazette-liquidation'
    }))
  )

  await p.run().waitUntilFinish()
  debug('pipeline complete')
}

main()