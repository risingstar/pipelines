const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { HttpLink } = require('apollo-link-http')
const fetch = require('node-fetch')

/**
 * A GraphQL client preconfigured with:
 *
 * - an in-memory cache;
 * - the 'node-fetch' module.
 */
class ApiClient extends ApolloClient {
  constructor(uri) {
    super({
      link: new HttpLink({ uri, fetch }),
      cache: new InMemoryCache()
    })
  }
}

module.exports = ApiClient
