/**
 * This is the GraphQL schema definition which includes any required resolvers,
 * such as data types.
 */
const { gql } = require('apollo-server')
const { Kind } = require('graphql/language')
const { GraphQLScalarType } = require('graphql')

/**
 * The GraphQL schema definition.
 *
 * Any custom types must have a resolver in the next secion.
 */
const typeDefs = gql`
  type Query {
    Organization(identifier: String): [Organization]
    Organizations(orgStatus: String): [Organization]
    OrganizationFeed(orgStatus: String, cursor: String, size: Int): OrganizationFeed
    PostCodeUnit(postCode: String): PostCodeUnit
    Prediction(orgAge: Int, orgCategory: String, orgEmployees: Int, orgProfitAndLoss: Int, orgType: String, rurInd: String): Prediction
  }

  type OrganizationFeed {
    cursor: String
    organizations: [Organization]
  }

  type Organization {
    "The official name of the organization, e.g. the registered company name."
    legalName: String
    "The identifier property represents any kind of identifier for any kind of Thing, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links."
    identifier: String
    "This property records the type of company. Familiar types are SA, PLC, LLC, GmbH etc. At the time of publication, there is no agreed set of company types that crosses borders. The term 'SA' is used in Poland and France for example although they mean slightly different things. The UK's LLP and Greece's EPE provide further example of close, but not exact, matches."
    orgType: String
    "Recording the status of an organization presents the same issues as its type. The terms 'insolvent', 'bankrupt' and 'in receivership,' for example, are likely to mean slightly different things with different legal implications in different jurisdictions."
    orgStatus: String
    "An object valued property of a Registered Company that associates a RegisteredCompany with its registered address."
    registeredAddress: Address
  }

  "Address."
  type Address {
    "The postal code associated with the address of the object"
    postalCode: String
    postCodeUnit: PostCodeUnit
  }

  type PostCodeUnit {
    ru11Ind: String
  }

  type Prediction {
    risk: Int
  }
`

/**
 * Resolvers for any custom types defined in the schema above.
 *
 * TODO(MB): Consider factoring this, and the scalar definition above, so that
 * it is always present and available to any schema.
 */
const typeResolvers = { }

module.exports = {
  typeDefs,
  typeResolvers
}
