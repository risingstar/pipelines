class Feature extends Map {
  constructor(args) {
    /**
     * Create the Map in whatever way the caller wants:
     */
    super(args)

    /**
     * Once constructed find the maximum value:
     */
    this.max = Math.max(...this.values())
  }

  /**
   * Use a key to lookup its mapped value.
   */
  normalise(key) {
    const value = this.get(key)

    if (value === undefined) {
      throw new Error(
        `There is no mapping for the key '${key}' in the map ${JSON.stringify(Array.from(this))}`
      )
    }

    return value
  }

  _oneHot(value, range) {
    /**
     * Create an array the length of the range:
     */
    const arr = new Array(range)
    /**
     * Fill it with zeroes:
     */
    arr.fill(0)
    /**
     * Set the element at the required position to 1:
     */
    arr[value] = 1

    /**
     * Reutrn the array:
     */
    return arr
  }

  oneHot(x) {
    /**
     * The size of this map gives us the range of possible values:
     */
    return this._oneHot(x, this.size)
  }

  /**
   * Scale the value between zero and 1.
   *
   * @param value Value to be normalised.
   */
  scale0To1(value, max=this.max) {

    /**
     * Return the value normalised within the range:
     */
    return value / max
  }
}

module.exports = Feature
