const Feature = require('./Feature')


const orgStatusMap = new Feature([
  ['Active', 0],

  ['Inactive', 1]
])

module.exports = orgStatusMap
