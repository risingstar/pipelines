/**
 * Sets up the datasources:
 */
const dataSources = () => {
  /**
   * Load a datasource that gets its data from ElasticSearch:
   */
  const ElasticSearchDataSource = require('./ElasticSearchDataSource')
  const esSource = new ElasticSearchDataSource(
    {
      identifier: 'CompanyNumber',
      legalName: 'CompanyName',
      orgStatus: 'CompanyStatus',
      orgType: 'CompanyCategory',
      postalCode: 'RegAddress.PostCode'
    },
    {
      host: process.env.ELASTICSEARCH_HOST
    }
  )
  /**
   * Load a datasource that makes predictions:
   */
  const PredictionDataSource = require('./PredictionDataSource')
  const predictionSource = new PredictionDataSource()
  return {
    query: {
      ...esSource.query,
      ...predictionSource.query
    },
    type: {
      ...esSource.type,
      ...predictionSource.type
    }
  }
}

module.exports = dataSources
