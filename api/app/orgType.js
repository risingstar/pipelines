const Feature = require('./Feature')

/**
 * Normalise the organisation types into groups of similar types of
 * company:
 */
const orgTypeMap = new Feature([
  ['Public limited company - AD', 0],
  ['One-person public limited company - EAD', 0],

  ['Private limited company - OOD', 1],
  ['One-person private limited company - EOOD', 1],

  ['Sole proprietorship - ET', 2],

  ['Limited partnership', 3],
  ['General partnership', 3],

  ['Cooperative', 4]
])

module.exports = orgTypeMap
