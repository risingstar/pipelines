/**
 * Load a datasource that gets its data from ElasticSearch:
 */
const ElasticSearchSource = require('./ElasticSearchSource')

/**
 * This is very hacky but it does allow us to pass all
 * arguments from the GraphQL call, straight through to
 * the target method.
 *
 * The problem with it though is that the 'from' parameter
 * names are in Schema.org world, but the 'to' names are
 * just parameter names, as used in functions being called.
 * It would be better if these names matched what was being
 * queried.
 */
const mapArgs = args => {
  const mappedArgs = {}

  for (const arg of Object.keys(args)) {
    switch (arg) {
      case 'identifier':
        mappedArgs['companyNumber'] = args[arg]
        break
      case 'orgStatus':
        mappedArgs['companyStatus'] = args[arg]
        break
      case 'postalCode':
        mappedArgs['postCode'] = args[arg]
        break
      default:
        mappedArgs[arg] = args[arg]
        break
    }
  }
  return mappedArgs
}

/**
 * An ElasticSearch datasource:
 */
class ElasticSearchDataSource {
  constructor(mappings, config) {
    this.esSource = new ElasticSearchSource(config)
    this.mappings = mappings
    this.query = {
      Organization: args => this.esSource.getCompany(mapArgs(args)),
      OrganizationFeed: args => this.esSource.getCompaniesFeed(mapArgs(args)),
      Organizations: args => this.esSource.getCompanies(mapArgs(args)),
      PostCodeUnit: args => this.esSource.getPostCodeUnit(mapArgs(args))
    }
  }

  get type() {
    const parent = this

    return {
      get Organization() {
        return class Organization {
          constructor(company) {
            this.company = company
            this.mappings = parent.mappings

            /**
             * Proxy the object we're creating with a simple handler that
             * maps the fields from a schema.org Organization, to a Companies
             * House company:
             */

            return new Proxy(this, {
              get: function(obj, prop) {
                /**
                 * If the caller is just trying to determine if we are a Promise
                 * then the answer depends on whether the contained object is a
                 * Promise:
                 */
                if (prop === 'then') {
                  return obj.company.then
                }

                /**
                 * Get the name of the field that the property maps to:
                 */

                let mappedFieldName = obj.mappings[prop]

                /**
                 * If there is no mapping AND there is no property with
                 * the same name as the original 'prop' then throw an error:
                 */

                if (!mappedFieldName) {
                  if (!obj.company[prop]) {
                    throw new ReferenceError(`There is no field mapping for '${prop}'`)
                  }
                  mappedFieldName = prop
                }

                /**
                 * Return the value of the mapped field. Note that we don't check
                 * that there is a value, since 'undefined' is a legitimate response:
                 */

                return obj.company[mappedFieldName]
              }
            })
          }
        }
      },

      get PostCodeUnit() {
        return class PostCodeUnit {
          constructor(postCodeUnit) {
            this.postCodeUnit = postCodeUnit
            this.mappings = parent.mappings

            /**
             * Proxy the object we're creating with a simple handler that
             * maps the fields from a schema.org Organization, to a Companies
             * House company:
             */

            return new Proxy(this, {
              get: function(obj, prop) {
                /**
                 * If the caller is just trying to determine if we are a Promise
                 * then the answer depends on whether the contained object is a
                 * Promise:
                 */
                if (prop === 'then') {
                  return obj.postCodeUnit.then
                }

                /**
                 * Get the name of the field that the property maps to:
                 */

                let mappedFieldName = obj.mappings[prop]

                /**
                 * If there is no mapping AND there is no property with
                 * the same name as the original 'prop' then throw an error:
                 */

                if (!mappedFieldName) {
                  if (!obj.postCodeUnit[prop]) {
                    throw new ReferenceError(`There is no field mapping for '${prop}'`)
                  }
                  mappedFieldName = prop
                }

                /**
                 * Return the value of the mapped field. Note that we don't check
                 * that there is a value, since 'undefined' is a legitimate response:
                 */

                return obj.postCodeUnit[mappedFieldName]
              }
            })
          }
        }
      }
    }
  }
}

module.exports = ElasticSearchDataSource
