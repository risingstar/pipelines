/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html } from '@polymer/lit-element';

import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';

// This element is *not* connected to the Redux store.
class OrgType extends LitElement {
  constructor() {
    super()
    this.lookup = new Map([
      ['Public limited company - AD', 0],
      ['One-person public limited company - EAD', 0],

      ['Private limited company - OOD', 1],
      ['One-person private limited company - EOOD', 1],

      ['Sole proprietorship - ET', 2],

      ['Limited partnership', 3],
      ['General partnership', 3],

      ['Cooperative', 4]
    ])
  }

  _onValueChanged(e) {
    console.log('orgType', this.lookup.get(e.detail.value))
    this.dispatchEvent(new CustomEvent(
      'org-type-changed',
      {
        detail: {
          value: this.lookup.get(e.detail.value)
        }
      }
    ));
  }

  render() {
    return html`
      <style>
        :host { display: block; }
      </style>
      <paper-dropdown-menu label="orgType" @value-changed="${this._onValueChanged}">
        <paper-listbox slot="dropdown-content" selected="0">
          <paper-item>Public limited company - AD</paper-item>
          <paper-item>One-person public limited company - EAD</paper-item>

          <paper-item>Private limited company - OOD</paper-item>
          <paper-item>One-person private limited company - EOOD</paper-item>

          <paper-item>Sole proprietorship - ET</paper-item>

          <paper-item>Limited partnership</paper-item>
          <paper-item>General partnership</paper-item>

          <paper-item>Cooperative</paper-item>
        </paper-listbox>
      </paper-dropdown-menu>
    `;
  }
}

window.customElements.define('org-type', OrgType);
