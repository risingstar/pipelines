/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html } from '@polymer/lit-element';

import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';

// This element is *not* connected to the Redux store.
class OrgCategory extends LitElement {
  constructor() {
    super()
    this.lookup = new Map([
      ['01', 0],
      ['02', 1],
      ['03', 2],
      ['05', 3],
      ['06', 4],
      ['07', 5],
      ['08', 6],
      ['09', 7],
      ['10', 8],
      ['11', 9],
      ['12', 10],
      ['13', 11],
      ['14', 12],
      ['15', 13],
      ['16', 14],
      ['17', 15],
      ['18', 16],
      ['19', 17],
      ['20', 18],
      ['21', 19],
      ['22', 20],
      ['23', 21],
      ['24', 22],
      ['25', 23],
      ['26', 24],
      ['27', 25],
      ['28', 26],
      ['29', 27],
      ['30', 28],
      ['31', 29],
      ['32', 30],
      ['33', 31],
      ['35', 32],
      ['36', 33],
      ['37', 34],
      ['38', 35],
      ['39', 36],
      ['41', 37],
      ['42', 38],
      ['43', 39],
      ['45', 40],
      ['46', 41],
      ['47', 42],
      ['49', 43],
      ['50', 44],
      ['51', 45],
      ['52', 46],
      ['53', 47],
      ['55', 48],
      ['56', 49],
      ['58', 50],
      ['59', 51],
      ['60', 52],
      ['61', 53],
      ['62', 54],
      ['63', 55],
      ['64', 56],
      ['65', 57],
      ['66', 58],
      ['68', 59],
      ['69', 60],
      ['70', 61],
      ['71', 62],
      ['72', 63],
      ['73', 64],
      ['74', 65],
      ['75', 66],
      ['77', 67],
      ['78', 68],
      ['79', 69],
      ['80', 70],
      ['81', 71],
      ['82', 72],
      ['85', 73],
      ['86', 74],
      ['87', 75],
      ['90', 76],
      ['92', 77],
      ['93', 78],
      ['94', 79],
      ['95', 80],
      ['96', 81]
    ])
  }

  _onValueChanged(e) {
    console.log('orgCategory', this.lookup.get(e.detail.value))
    this.dispatchEvent(new CustomEvent(
      'org-category-changed',
      {
        detail: {
          value: this.lookup.get(e.detail.value)
        }
      }
    ));
  }

  render() {
    return html`
      <style>
        :host { display: block; }
      </style>
      <paper-dropdown-menu label="orgCategory" @value-changed="${this._onValueChanged}">
        <paper-listbox slot="dropdown-content" selected="0">
          <paper-item value="0">01</paper-item>
          <paper-item value="1">02</paper-item>
          <paper-item value="2">03</paper-item>
          <paper-item value="3">05</paper-item>
          <paper-item value="4">06</paper-item>
          <paper-item value="5">07</paper-item>
          <paper-item value="6">08</paper-item>
          <paper-item value="7">09</paper-item>
          <paper-item value="8">10</paper-item>
          <paper-item value="9">11</paper-item>
          <paper-item value="10">12</paper-item>
          <paper-item value="11">13</paper-item>
          <paper-item value="12">14</paper-item>
          <paper-item value="13">15</paper-item>
          <paper-item value="14">16</paper-item>
          <paper-item value="15">17</paper-item>
          <paper-item value="16">18</paper-item>
          <paper-item value="17">19</paper-item>
          <paper-item value="18">20</paper-item>
          <paper-item value="19">21</paper-item>
          <paper-item value="20">22</paper-item>
          <paper-item value="21">23</paper-item>
          <paper-item value="22">24</paper-item>
          <paper-item value="23">25</paper-item>
          <paper-item value="24">26</paper-item>
          <paper-item value="25">27</paper-item>
          <paper-item value="26">28</paper-item>
          <paper-item value="27">29</paper-item>
          <paper-item value="28">30</paper-item>
          <paper-item value="29">31</paper-item>
          <paper-item value="30">32</paper-item>
          <paper-item value="31">33</paper-item>
          <paper-item value="32">35</paper-item>
          <paper-item value="33">36</paper-item>
          <paper-item value="34">37</paper-item>
          <paper-item value="35">38</paper-item>
          <paper-item value="36">39</paper-item>
          <paper-item value="37">41</paper-item>
          <paper-item value="38">42</paper-item>
          <paper-item value="39">43</paper-item>
          <paper-item value="40">45</paper-item>
          <paper-item value="41">46</paper-item>
          <paper-item value="42">47</paper-item>
          <paper-item value="43">49</paper-item>
          <paper-item value="44">50</paper-item>
          <paper-item value="45">51</paper-item>
          <paper-item value="46">52</paper-item>
          <paper-item value="47">53</paper-item>
          <paper-item value="48">55</paper-item>
          <paper-item value="49">56</paper-item>
          <paper-item value="50">58</paper-item>
          <paper-item value="51">59</paper-item>
          <paper-item value="52">60</paper-item>
          <paper-item value="53">61</paper-item>
          <paper-item value="54">62</paper-item>
          <paper-item value="55">63</paper-item>
          <paper-item value="56">64</paper-item>
          <paper-item value="57">65</paper-item>
          <paper-item value="58">66</paper-item>
          <paper-item value="59">68</paper-item>
          <paper-item value="60">69</paper-item>
          <paper-item value="61">70</paper-item>
          <paper-item value="62">71</paper-item>
          <paper-item value="63">72</paper-item>
          <paper-item value="64">73</paper-item>
          <paper-item value="65">74</paper-item>
          <paper-item value="66">75</paper-item>
          <paper-item value="67">77</paper-item>
          <paper-item value="68">78</paper-item>
          <paper-item value="69">79</paper-item>
          <paper-item value="70">80</paper-item>
          <paper-item value="71">81</paper-item>
          <paper-item value="72">82</paper-item>
          <paper-item value="73">85</paper-item>
          <paper-item value="74">86</paper-item>
          <paper-item value="75">87</paper-item>
          <paper-item value="76">90</paper-item>
          <paper-item value="77">92</paper-item>
          <paper-item value="78">93</paper-item>
          <paper-item value="79">94</paper-item>
          <paper-item value="80">95</paper-item>
          <paper-item value="81">96</paper-item>
        </paper-listbox>
      </paper-dropdown-menu>
    `;
  }
}

window.customElements.define('org-category', OrgCategory);
