const tap = require('tap')
tap.comment('resolvers for Schema.org/Organization')

const { gql } = require('apollo-server')
const ApiServer = require('../app/ApiServer')
const ApiClient = require('../app/ApiClient')

const main = async () => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  tap.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  let data = await client.query({
    query: gql`
      {
        Organization(identifier: "07716384") {
          legalName,
          identifier
          registeredAddress {
            postalCode
          }
        }
      }
    `
  })
  tap.same(
    data.data,
    {
      Organization: [{
        __typename: 'Organization',
        legalName: 'IDEA IS EVERYTHING LIMITED',
        identifier: '07716384',
        registeredAddress: {
          __typename: 'Address',
          'postalCode': 'E8 3PA'
        }
      }]
    }
  )

  tap.comment('Regression test for issue #8')
  /**
   * We know this company has no postcode:
   */
  data = await client.query({
    query: gql`
      {
        Organization(identifier: "04766030") {
          registeredAddress {
            postalCode
            postCodeUnit {
              ru11Ind
            }
          }
        }
      }
    `
  })
  tap.same(
    data.data,
    {
      Organization: [{
        registeredAddress: {
          __typename: 'Address',
          postalCode: '',
          postCodeUnit: null
        },
        __typename: 'Organization'
      }]
    }
  )
  server.close()
}

main()
