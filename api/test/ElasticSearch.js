const tap = require('tap')
tap.comment('query for Schema.org/Organization on ElasticSearch')

const { ApolloServer, gql, makeExecutableSchema} = require('apollo-server')
const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { HttpLink } = require('apollo-link-http')
const fetch = require('node-fetch')

const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://localhost:4000', fetch }),
  cache: new InMemoryCache()
})

const ElasticSearchSource = require('../app/ElasticSearchSource')
const esSource = new ElasticSearchSource({ host: process.env.ELASTICSEARCH_HOST })

const organizationResolver = {
  query: {
    Organization: (_source, args, { dataSources }) => {
      return dataSources.getOrganization(args)
    }
  },
  type: {
    Organization: {
      legalName: _source => _source.CompanyName,
      identifier: _source => _source.CompanyNumber
    }
  }
}

tap.test('query for Engine House', async t => {
  const typeDefs = gql`
    type Query {
      Organization(identifier: String): [Organization]
    }

    type Organization {
      "The official name of the organization, e.g. the registered company name."
      legalName: String
      "The identifier property represents any kind of identifier for any kind of Thing, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links."
      identifier: String
    }
  `

  const resolvers = {
    Query: {
      ...organizationResolver.query
    },
    ...organizationResolver.type
  }

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers
  })

  const mapArgs = args => {
    const mappedArgs = {}

    for (const arg of Object.keys(args)) {
      if (arg === 'identifier') {
        mappedArgs['companyNumber'] = args.identifier
      } else {
        mappedArgs[arg] = args[arg]
      }
    }
    return mappedArgs
  }

  const api = new ApolloServer({
    schema,
    dataSources: () => ({
      getOrganization: args => esSource.getCompany(mapArgs(args))
    })
  })

  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const data = await client.query({
    query: gql`
      {
        Organization(identifier: "07716384") {
          legalName,
          identifier
        }
      }
    `
  })
  t.same(
    data.data,
    {
      Organization: [{
        __typename: 'Organization',
        legalName: 'IDEA IS EVERYTHING LIMITED',
        identifier: '07716384'
      }]
    }
  )
  server.close()
})
