const tap = require('tap')
tap.comment('check resolver for rural/urban indicator')

const { gql } = require('apollo-server')
const ApiServer = require('../app/ApiServer')
const ApiClient = require('../app/ApiClient')

tap.test('query for 1 organisation', async t => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  const data = await client.query({
    query: gql`
      {
        Organization(identifier: "07845805") {
          legalName
          identifier
          registeredAddress {
            postalCode
            postCodeUnit {
              ru11Ind
            }
          }
        }
      }
    `
  })
  t.same(
    data.data,
    {
      Organization: [{
        __typename: 'Organization',
        legalName: 'INTERMARQUE BESPOKE LIMITED',
        identifier: '07845805',
        registeredAddress: {
          __typename: 'Address',
          postalCode: 'M1 3BE',
          postCodeUnit: {
            __typename: 'PostCodeUnit',
            ru11Ind: 'A1'
          }
        }
      }]
    }
  )
  server.close()
  t.end()
})

tap.test('query for multiple organisations', async t => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  const data = await client.query({
    query: gql`
      {
        Organizations(orgStatus: "Liquidation") {
          legalName
          identifier
          registeredAddress {
            postalCode
            postCodeUnit {
              ru11Ind
            }
          }
        }
      }
    `
  })
  t.same(data.data.Organizations.length, 10)
  server.close()
  t.end()
})
