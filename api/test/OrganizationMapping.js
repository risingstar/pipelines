const tap = require('tap')
tap.comment('map Schema.org/Organization to Companies House format')

const { ApolloServer, gql, makeExecutableSchema} = require('apollo-server')
const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { HttpLink } = require('apollo-link-http')
const fetch = require('node-fetch')

const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://localhost:4000', fetch }),
  cache: new InMemoryCache()
})

const ElasticSearchSource = require('../app/ElasticSearchSource')
const esSource = new ElasticSearchSource({ host: process.env.ELASTICSEARCH_HOST })

const organizationResolver = {
  query: {
    Organization: (_, args, { dataSources }) => {
      return dataSources.Organization.source(args)
    }
  },
  type: {
    Organization: {
      legalName: (parent, _, { dataSources }) => {
        return dataSources.Organization.mapping(parent, 'legalName')
      },
      identifier: (parent, _, { dataSources }) => {
        return dataSources.Organization.mapping(parent, 'identifier')
      },
      orgType: (parent, _, { dataSources }) => {
        return dataSources.Organization.mapping(parent, 'orgType')
      },
      orgStatus: (parent, _, { dataSources }) => {
        return dataSources.Organization.mapping(parent, 'orgStatus')
      }
    }
  }
}

tap.test('query for Idea is Everything', async t => {
  const typeDefs = gql`
    type Query {
      Organization(identifier: String): [Organization]
    }

    type Organization {
      "The official name of the organization, e.g. the registered company name."
      legalName: String
      "The identifier property represents any kind of identifier for any kind of Thing, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links."
      identifier: String
      "This property records the type of company. Familiar types are SA, PLC, LLC, GmbH etc. At the time of publication, there is no agreed set of company types that crosses borders. The term 'SA' is used in Poland and France for example although they mean slightly different things. The UK's LLP and Greece's EPE provide further example of close, but not exact, matches."
      orgType: String
      "Recording the status of an organization presents the same issues as its type. The terms 'insolvent', 'bankrupt' and 'in receivership,' for example, are likely to mean slightly different things with different legal implications in different jurisdictions."
      orgStatus: String
    }
  `

  const resolvers = {
    Query: {
      ...organizationResolver.query
    },
    ...organizationResolver.type
  }

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers
  })

  const mapArgs = args => {
    const mappedArgs = {}

    for (const arg of Object.keys(args)) {
      if (arg === 'identifier') {
        mappedArgs['companyNumber'] = args.identifier
      } else {
        mappedArgs[arg] = args[arg]
      }
    }
    return mappedArgs
  }

  const api = new ApolloServer({
    schema,
    dataSources: () => ({
      Organization: {
        source: args => esSource.getCompany(mapArgs(args)),
        mapping: (_source, fieldName) => {
          switch (fieldName) {
            case 'legalName': return _source.CompanyName
            case 'identifier': return _source.CompanyNumber
          }
        }
      }
    })
  })

  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const data = await client.query({
    query: gql`
      {
        Organization(identifier: "07716384") {
          legalName,
          identifier
        }
      }
    `
  })
  t.same(
    data.data,
    {
      Organization: [{
        __typename: 'Organization',
        legalName: 'IDEA IS EVERYTHING LIMITED',
        identifier: '07716384'
      }]
    }
  )
  server.close()
})
