const tap = require('tap')
tap.comment('resolvers for ONS Post Code Unit')

const { gql } = require('apollo-server')
const ApiServer = require('../app/ApiServer')
const ApiClient = require('../app/ApiClient')

const main = async () => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  tap.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  let data = await client.query({
    query: gql`
      {
        PostCodeUnit(postCode: "E8 3PA") {
          ru11Ind
        }
      }
    `
  })
  tap.same(
    data.data,
    {
      PostCodeUnit: {
        __typename: 'PostCodeUnit',
        ru11Ind: 'A1'
      }
    }
  )

  tap.comment('Regression test for issue #5')
  data = await client.query({
    query: gql`
      {
        PostCodeUnit(postCode: "SO21 1TH") {
          ru11Ind
        }
      }
    `
  })
  tap.same(
    data.data,
    {
      PostCodeUnit: {
        __typename: 'PostCodeUnit',
        ru11Ind: 'D1'
      }
    }
  )
  server.close()
}

main()
