import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import 'ag-grid-polymer';

var monthCellClassRules = {
    'no-risk': 'x === \'none\'',
    'very-low-risk': 'x === \'very low\'',
    'low-risk': 'x === \'low\'',
    'medium-risk': 'x === \'medium\'',
    'high-risk': 'x === \'high\''
};

/**
 * @customElement
 * @polymer
 */
class PwaApp extends PolymerElement {
  constructor() {
    super();

    this.columnDefs = [
      {
        headerName: "Company Name",
        field: "name"
      },
      {
        headerName: "Status",
        field: "status"
      },
      {
        headerName: "Risk Level",
        field: "riskLevel",
        cellClassRules: monthCellClassRules
      },
      {
        headerName: "Risk Factor",
        field: "risk"
      },
    ];
  }

  firstDataRendered(params) {
    params.api.sizeColumnsToFit()
  }

  gridReady() {
    const httpRequest = new XMLHttpRequest()
    httpRequest.open('GET', '/src/pwa-app/companies-predict.json')
    httpRequest.send()
    httpRequest.onreadystatechange = () => {
      if (httpRequest.readyState === 4 && httpRequest.status === 200) {
        const httpResult = JSON.parse(httpRequest.responseText)

        this.api.setRowData(httpResult)
      }
    }
  }

  static get template() {
    return html`
    <link rel="stylesheet" href="../../node_modules/ag-grid-community/dist/styles/ag-grid.css">
    <link rel="stylesheet" href="../../node_modules/ag-grid-community/dist/styles/ag-theme-balham.css">
    <link rel="stylesheet" href="../../src/pwa-app/style-risk.css">

    <ag-grid-polymer style="width: 100%; height: 350px;"
                     class="ag-theme-balham"
                     rowData="{{rowData}}"
                     columnDefs="{{columnDefs}}"
                     enableFilter
                     enableSorting
                     on-first-data-rendered="{{firstDataRendered}}"
                     on-grid-ready="{{gridReady}}"
    ></ag-grid-polymer>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'pwa-app'
      }
    };
  }
}

window.customElements.define('pwa-app', PwaApp);
